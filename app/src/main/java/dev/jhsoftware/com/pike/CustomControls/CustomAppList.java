package dev.jhsoftware.com.pike.CustomControls;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import dev.jhsoftware.com.pike.R;

public class CustomAppList extends LinearLayout {

    ImageView app_icon;
    TextView app_name, app_package;

    private String packageName;

    public CustomAppList(Context context, AttributeSet attrs){
        super(context, attrs);
        initControl(context);
    }

    public CustomAppList(Context context) {
        super(context);
        initControl(context);
    }

    private void initControl(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_app_list, this);

        findViews();
    }


    private void findViews() {
        app_icon = (ImageView) findViewById(R.id.icon);
        app_name = (TextView) findViewById(R.id.name);
        app_package = (TextView) findViewById(R.id.appPackage);
    }

    public void setData(String name, Drawable icon, String packageName, String appPackage){
        app_icon.setImageDrawable(icon);
        app_name.setText(name);
        app_package.setText(appPackage);
        this.packageName = packageName;
    }


    public String getPackageName() {
        return packageName;
    }



}
