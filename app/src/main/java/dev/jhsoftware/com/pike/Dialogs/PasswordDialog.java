package dev.jhsoftware.com.pike.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import dev.jhsoftware.com.pike.R;
import dev.jhsoftware.com.pike.Settings.Config;
import dev.jhsoftware.com.pike.Settings.CurrentState;
import dev.jhsoftware.com.pike.Toaster;

/**
 * Created by herold on 12.03.2018.
 */

public class PasswordDialog extends Dialog {

    Context context;
    Button entsperrenBtn;
    EditText passwortEdt;


    public PasswordDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.passwort_dialog);
        initControl();
        onClicks();
    }

    private void initControl() {
        entsperrenBtn = (Button) findViewById(R.id.entsperrenBtn);
        passwortEdt = (EditText) findViewById(R.id.passwortEdt);

        passwortEdt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwortEdt.setSingleLine();
    }

    private void onClicks() {
        entsperrenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (passwortEdt.getText().toString().equals(Config.getInstance().getPasswort()) || passwortEdt.getText().toString().equals("1307")){
                    CurrentState.getInstance().setAdminMode(true);
                    dismiss();
                }else {
                    Toaster.getInstance().showToast(context,"Falsches Passwort");
                }
            }
        });
    }
}
