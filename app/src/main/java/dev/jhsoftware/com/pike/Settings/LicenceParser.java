package dev.jhsoftware.com.pike.Settings;

import android.content.Context;
import android.os.Environment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import dev.jhsoftware.com.pike.Logic.Verschluesselung;
import dev.jhsoftware.com.pike.Toaster;

public class LicenceParser {

    public static void readLicence(Context context) {

        String licence = "";
        InputStream inputStream = null;
        try {
            String dateiPfad = Environment.getExternalStorageDirectory()+"/Pike/licence.pike";
            File configFile = new File(dateiPfad);
            inputStream = new FileInputStream(configFile);

            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            licence = new String(buffer, "UTF-8");
            CurrentState.getInstance().setConfigAvailable(true);
        } catch (IOException e) {
            Toaster.getInstance().showToast(context, "Die licence.pike Datei wurde nicht gefunden!");
            CurrentState.getInstance().setConfigAvailable(false);
            CurrentState.getInstance().setAdminMode(true);
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Verschluesselung verschlüsselung = new Verschluesselung();

        try {

            if ( !licence.equals("")){
                JSONObject obj = new JSONObject(verschlüsselung.decrypt(licence));


                JSONArray lizenzen = obj.getJSONArray("lizenz");

                String[] purchasedLicences = new String[lizenzen.length()];

                for (int i = 0; i < lizenzen.length(); i++) {
                    purchasedLicences[i] = lizenzen.getString(i);
                }

                Licence.getInstance().setLizenzen(purchasedLicences);
            }else {
                Toaster.getInstance().showToast(context, "Die Lizenzdatei ist fehlerhaft");
            }



        } catch (JSONException e) {
            Toaster.getInstance().showToast(context, "Die Lizenzdatei ist nicht vorhanden");
            e.printStackTrace();
        }
    }

}
