package dev.jhsoftware.com.pike;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by herold on 27.02.2018.
 */

public class Toaster {
    private static final Toaster ourInstance = new Toaster();

    public static Toaster getInstance() {
        return ourInstance;
    }

    private Toaster() {


    }

    public void showToast(Context context, String nachricht){
        Toast.makeText(context, nachricht, Toast.LENGTH_SHORT).show();
    }
}
