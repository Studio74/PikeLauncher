package dev.jhsoftware.com.pike.CustomControls;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import dev.jhsoftware.com.pike.R;

/**
 * Created by herold on 14.03.2018.
 */

public class CustomApp extends LinearLayout {

    ImageView app_icon;
    TextView app_name;

    private String packageName;
    private String className;

    public CustomApp(Context context, AttributeSet attrs){
        super(context, attrs);
        initControl(context);
    }

    public CustomApp(Context context) {
        super(context);
        initControl(context);
    }

    private void initControl(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_app, this);

        findViews();
    }

    private void findViews() {
        app_icon = (ImageView) findViewById(R.id.icon);
        app_name = (TextView) findViewById(R.id.name);
    }

    public void setData(String name, Drawable icon, String packageName, String className){
        app_icon.setImageDrawable(icon);
        app_name.setText(name);
        this.packageName = packageName;
        this.className = className;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassName() { return className; }
}
