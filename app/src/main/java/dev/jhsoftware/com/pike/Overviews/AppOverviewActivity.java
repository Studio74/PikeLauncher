package dev.jhsoftware.com.pike.Overviews;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import dev.jhsoftware.com.pike.Adapter;
import dev.jhsoftware.com.pike.CustomControls.CustomApp;
import dev.jhsoftware.com.pike.R;

public class AppOverviewActivity extends Activity {

    private PackageManager packageManager = null;

    private List<ApplicationInfo> applist = null;
    private LinearLayout linearLayout;
    private Adapter listadaptor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_overview);

        packageManager = getPackageManager();

        linearLayout = (LinearLayout) findViewById(R.id.appList);


        checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));


    }


    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();


        int applicationZähler = 0;
        int applicationMenge = list.size();

        while (applicationZähler != applicationMenge) {

            LinearLayout newLine = new LinearLayout(getApplicationContext());
            newLine.setOrientation(LinearLayout.HORIZONTAL);
            newLine.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            int position = 0;
            while (position != 4) {
                position++;
                try {

                    ApplicationInfo info = list.get(applicationZähler);

                    final CustomApp customApp = new CustomApp(getApplicationContext());
                    customApp.setData(info.loadLabel(packageManager).toString(), info.loadIcon(packageManager), info.packageName, info.name);

                    customApp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            try{
                                Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(customApp.getPackageName());
                                startActivity(intent);
                            }catch (Exception e){
                                e.printStackTrace();
                            }



                        }
                    });

                    newLine.addView(customApp);
                    applicationZähler++;


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            linearLayout.addView(newLine);

        }



        return applist;
    }


}
