package dev.jhsoftware.com.pike.Logic;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.format.Time;

import com.google.common.base.MoreObjects;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dev.jhsoftware.com.pike.Settings.Licence;
import dev.jhsoftware.com.pike.Settings.LicenceParser;
import dev.jhsoftware.com.pike.Toaster;

/**
 * Created by herold on 19.03.2018.
 */

public class LizenzLogic {
    private static final LizenzLogic ourInstance = new LizenzLogic();
    Context context;

    public static LizenzLogic getInstance() {
        return ourInstance;
    }

    private LizenzLogic() {
    }


    public Boolean checkLizenz(Context context) {


        this.context = context;

        LicenceParser.readLicence(context);

        String seriennummer = Build.SERIAL;

        for (int i = 0; i < Licence.getInstance().getLizenzen().length; i++) {

            if (Licence.getInstance().getLizenzen()[i].contains(seriennummer)) {


                String eingebetteteLizenz = Licence.getInstance().getLizenzen()[i].substring(Licence.getInstance().getLizenzen()[i].indexOf("#") + 1, Licence.getInstance().getLizenzen()[i].lastIndexOf("#"));
                String datum = Licence.getInstance().getLizenzen()[i].substring(Licence.getInstance().getLizenzen()[i].lastIndexOf("#") + 1);

                if (Licence.getInstance().getLizenzen()[i].contains("identWERKDemo")) {
                    return demoLizenzBerechnen(eingebetteteLizenz, datum);
                } else {
                    return lizenzBerechnen(eingebetteteLizenz, datum);
                }


            }
        }
        return false;


    }

    private Boolean lizenzBerechnen(String licence, String date) {

        String seriennummer = Build.SERIAL;
        seriennummer = seriennummer.replaceAll("[0a-zA-Z]", "");

        int ergebnis = 0;

        for (char zahl : seriennummer.toCharArray()) {
            ergebnis = ergebnis + Integer.parseInt(String.valueOf(zahl));
        }

        String multiplikator = Character.toString(seriennummer.charAt(1));

        ergebnis = Integer.parseInt(multiplikator) * ergebnis;

        if (ergebnis == Integer.parseInt(licence)) {
            return checkDatum(date);
        } else {
            Toaster.getInstance().showToast(context, "Die Lizenz ist nicht korrekt");
            return false;
        }
    }

    private Boolean checkDatum(String datum) {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = df.format(c);

        int erstellJahr = Integer.parseInt(datum.substring(datum.lastIndexOf(".") + 1));
        int aktuellesJahr = Integer.parseInt(formattedDate.substring(formattedDate.lastIndexOf(".") + 1));

        int erstellMonat = Integer.parseInt(datum.substring(datum.indexOf(".") + 1, datum.lastIndexOf(".")));
        int aktuellerMonat = Integer.parseInt(formattedDate.substring(formattedDate.indexOf(".") + 1, formattedDate.lastIndexOf(".")));


        if (aktuellesJahr == erstellJahr) {

            return true;

        } else {

            if (aktuellesJahr > erstellJahr) {
                if (aktuellerMonat < erstellMonat) {
                    return true;
                } else {
                    Toaster.getInstance().showToast(context, "Ihre Lizenz ist abgelaufen");
                    return false;
                }
            } else {
                Toaster.getInstance().showToast(context, "Ihre Lizenz ist abgelaufen");
                return false;
            }

        }

    }


    private Boolean demoLizenzBerechnen(String licence, String date) {

        licence = licence.replace("identWERKDemo", "");

        String seriennummer = Build.SERIAL;
        seriennummer = seriennummer.replaceAll("[0a-zA-Z]", "");

        int ergebnis = 0;

        for (char zahl : seriennummer.toCharArray()) {
            ergebnis = ergebnis + Integer.parseInt(String.valueOf(zahl));
        }

        String multiplikator = Character.toString(seriennummer.charAt(1));

        ergebnis = Integer.parseInt(multiplikator) * ergebnis;

        if (ergebnis == Integer.parseInt(licence)) {
            return demoCheckDatum(date);
        } else {
            Toaster.getInstance().showToast(context, "Die Lizenz ist nicht korrekt");
            return false;
        }
    }

    private Boolean demoCheckDatum(String datum) {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        String formattedDate = df.format(c);

        int erstellJahr = Integer.parseInt(datum.substring(datum.lastIndexOf(".") + 1));
        int aktuellesJahr = Integer.parseInt(formattedDate.substring(formattedDate.lastIndexOf(".") + 1));

        int erstellMonat = Integer.parseInt(datum.substring(datum.indexOf(".") + 1, datum.lastIndexOf(".")));
        int aktuellerMonat = Integer.parseInt(formattedDate.substring(formattedDate.indexOf(".") + 1, formattedDate.lastIndexOf(".")));


        if (aktuellesJahr == erstellJahr) {

            if (aktuellerMonat - 2 > erstellMonat) {
                Toaster.getInstance().showToast(context, "Ihre Demolizenz ist abgelaufen");
                return false;
            }


            return true;

        } else {


            Toaster.getInstance().showToast(context, "Ihre Demolizenz ist abgelaufen");
            return false;


        }

    }

}
