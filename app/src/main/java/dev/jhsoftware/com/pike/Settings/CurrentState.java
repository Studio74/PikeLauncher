package dev.jhsoftware.com.pike.Settings;

/**
 * Created by herold on 12.03.2018.
 */

public class CurrentState {

    private static final CurrentState ourInstance = new CurrentState();

    private Boolean adminMode = false;
    private Boolean allowOpening = false;
    private Boolean licenceAktive = false;
    private Boolean showPackages = false;
    private Boolean configAvailable = false;

    public static CurrentState getInstance() {
        return ourInstance;
    }

    private CurrentState() {

    }

    public Boolean getConfigAvailable() {
        return configAvailable;
    }
    public void setConfigAvailable(Boolean configAvailable) {
        this.configAvailable = configAvailable;
    }

    public Boolean getAdminMode() {
        return adminMode;
    }
    public void setAdminMode(Boolean adminMode) {
        this.adminMode = adminMode;
    }

    public Boolean getAllowOpening() {
        return allowOpening;
    }
    public void setAllowOpening(Boolean allowOpening) {
        this.allowOpening = allowOpening;
    }

    public Boolean getLicenceAktive() {
        return licenceAktive;
    }
    public void setLicenceAktive(Boolean licenceAktive) {
        this.licenceAktive = licenceAktive;
    }

    public Boolean getShowPackages() {
        return showPackages;
    }
    public void setShowPackages(Boolean showPackages) {
        this.showPackages = showPackages;
    }
}