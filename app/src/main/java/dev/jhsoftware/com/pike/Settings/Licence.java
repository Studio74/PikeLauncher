package dev.jhsoftware.com.pike.Settings;

public class Licence {

    private static final Licence ourInstance = new Licence();

    private String[] lizenzen;


    public static Licence getInstance() {
        return ourInstance;
    }

    private Licence() {



    }

    public String[] getLizenzen() {
        return lizenzen;
    }
    public void setLizenzen(String[] lizenzen) {
        this.lizenzen = lizenzen;
    }


}
