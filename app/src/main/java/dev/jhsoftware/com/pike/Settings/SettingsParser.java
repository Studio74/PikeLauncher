package dev.jhsoftware.com.pike.Settings;

import android.content.Context;
import android.os.Environment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import dev.jhsoftware.com.pike.Logic.Verschluesselung;
import dev.jhsoftware.com.pike.Toaster;


/**
 * Created by herold on 12.03.2018.
 */
public class SettingsParser {

    public static Boolean readConfig(Context context) {

        String test = "";
        InputStream inputStream = null;
        try {
            String dateiPfad = Environment.getExternalStorageDirectory()+"/Pike/config.pike";
            File configFile = new File(dateiPfad);
            inputStream = new FileInputStream(configFile);

            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            test = new String(buffer, "UTF-8");
            String hallo = test;
            CurrentState.getInstance().setConfigAvailable(true);
        } catch (IOException e) {
            Toaster.getInstance().showToast(context, "Die Config.pike Datei wurde nicht gefunden!");
            CurrentState.getInstance().setConfigAvailable(false);
            CurrentState.getInstance().setAdminMode(true);
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Verschluesselung verschlüsselung = new Verschluesselung();

        try {

            if ( !test.equals("")){
                JSONObject obj = new JSONObject(verschlüsselung.decrypt(test));

                Config.getInstance().setPasswort(obj.getString("adminpassword"));

                if (obj.getString("baseurl").contains("http://") || obj.getString("baseurl").contains("https://") ){
                    Config.getInstance().setURL(obj.getString("baseurl"));
                }else {
                    Config.getInstance().setURL("http://" + obj.getString("baseurl"));
                }
                Config.getInstance().setRefresh(true);
                Config.getInstance().setEnableBackButton(obj.getBoolean("enableBack"));
                Config.getInstance().setShowHomeButton(obj.getBoolean("enableHome"));
                Config.getInstance().setShowBrowserToolbar(obj.getBoolean("displayNavigationbar"));
                Config.getInstance().setEnterpriseBrowser(obj.getBoolean("enterprisebrowser"));
                Config.getInstance().setVersion(obj.getString("version"));
                Config.getInstance().setVersion(obj.getString("version"));
                Config.getInstance().setZoom(obj.getInt("scale"));
                Config.getInstance().setEnableZoom(obj.getBoolean("pinchtozoom"));
                Config.getInstance().setEnableJavascript(obj.getBoolean("enableJavascript"));
                Config.getInstance().setPinchToZoomButtons(obj.getBoolean("pinchtozoombuttons"));
                Config.getInstance().setDisplayMode(obj.getString("displaymode"));


                JSONArray apps = obj.getJSONArray("apps");

                String[] activeApps = new String[apps.length()];

                for (int i = 0; i < apps.length(); i++) {
                    activeApps[i] = apps.getString(i);
                }

                Config.getInstance().setString(activeApps);
                return true;
            }else {
                Toaster.getInstance().showToast(context, "Die Konfigurationsdatei ist fehlerhaft oder nicht vorhanden. Bitte wenden Sie sich an den Administrator");
                return false;
            }



        } catch (JSONException e) {
            Toaster.getInstance().showToast(context, "Die Konfigurationsdatei ist fehlerhaft oder nicht vorhanden. Bitte wenden Sie sich an den Administrator");

            e.printStackTrace();
            return false;
        }
    }
}
