package dev.jhsoftware.com.pike;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.app.admin.SystemUpdatePolicy;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.UserManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import dev.jhsoftware.com.pike.CustomControls.CustomApp;
import dev.jhsoftware.com.pike.CustomControls.CustomAppList;
import dev.jhsoftware.com.pike.Dialogs.PasswordDialog;
import dev.jhsoftware.com.pike.Logic.LizenzLogic;
import dev.jhsoftware.com.pike.Settings.Config;
import dev.jhsoftware.com.pike.Settings.CurrentState;
import dev.jhsoftware.com.pike.Settings.SettingsParser;

public class MainActivity extends Activity {

    private ImageButton infoBtn, exitAdminBtn, packageBtn, restartBtn;
    private LinearLayout adminViewLl, allAppList;
    private TextView versionTv;
    boolean currentFocus;
    PackageManager packageManager;
    boolean isPaused;
    Handler collapseNotificationHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //String seriennummer = Build.SERIAL;
        //Toaster.getInstance().showToast(getApplicationContext(), seriennummer);


        int ui = this.getWindow().getDecorView().getSystemUiVisibility();
        ui ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        ui ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        this.getWindow().getDecorView().setSystemUiVisibility(ui);


        if (Build.VERSION.SDK_INT >= 23) {
            int readCheck = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            if (readCheck != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }

            int writeCheck = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (writeCheck != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }

            int telefonCheck = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (telefonCheck != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            }
        }


        findViews();


        File file = new File(Environment.getExternalStorageDirectory() + "/Pike");
        if (!file.exists()) {
            if (!file.mkdirs()) {

            }
        }


        try {

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PikeLauncher", 0);

            if (LizenzLogic.getInstance().checkLizenz(getApplicationContext())) {
                CurrentState.getInstance().setLicenceAktive(true);
            } else {
                CurrentState.getInstance().setLicenceAktive(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (CurrentState.getInstance().getLicenceAktive()) {


            if (SettingsParser.readConfig(getApplicationContext())) {
                versionTv.setVisibility(View.GONE);
            }
            new LoadApplications().execute(MainActivity.this);
            restartBtn.setVisibility(View.GONE);
            packageBtn.setVisibility(View.GONE);


        } else {
            CurrentState.getInstance().setAdminMode(true);
            restartBtn.setVisibility(View.VISIBLE);
            infoBtn.setVisibility(View.VISIBLE);
            new LoadApplications().execute(MainActivity.this);
        }

        packageManager = getPackageManager();

        onClicks();

        if (Config.getInstance().getDisplayMode().equals("Portrait")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (Config.getInstance().getDisplayMode().equals("Landscape")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else if (Config.getInstance().getDisplayMode().equals("Auto")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return applist;
    }


    private void loadAllApps() {
        //allAppList.setVisibility(View.VISIBLE);
        allAppList.removeAllViews();

        allAppList.setVisibility(View.GONE);

        Intent activityIntent = new Intent(Intent.ACTION_MAIN, null);

        activityIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appliste = packageManager.queryIntentActivities(activityIntent, 0);


        //List<ResolveInfo> appliste = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));
        int applicationZähler = 0;

        int applicationMenge = appliste.size();

        while (applicationZähler != applicationMenge) {

            LinearLayout newLine = new LinearLayout(getApplicationContext());
            newLine.setOrientation(LinearLayout.HORIZONTAL);
            newLine.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            int position = 0;
            while (position != 3) {
                position++;
                try {

                    ResolveInfo info = appliste.get(applicationZähler);
                    LinearLayout weightLayout = new LinearLayout(getApplicationContext());
                    weightLayout.setOrientation(LinearLayout.HORIZONTAL);
                    weightLayout.setGravity(Gravity.CENTER);

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.weight = 1;
                    weightLayout.setLayoutParams(lp);



                    final CustomApp customApp = new CustomApp(getApplicationContext());
                    customApp.setData(info.loadLabel(packageManager).toString(), info.loadIcon(packageManager), info.activityInfo.packageName, info.activityInfo.name);

                    customApp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            CurrentState.getInstance().setAdminMode(true);

                            try {
                                //Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(customApp.getPackageName());
                                Intent intent = new Intent();
                                intent.setComponent(new ComponentName(customApp.getPackageName(), customApp.getClassName()));
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    weightLayout.addView(customApp);

                    newLine.addView(weightLayout);
                    applicationZähler++;

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            allAppList.addView(newLine);
        }

        allAppList.setVisibility(View.VISIBLE);

    }

    private void loadEnabledApps() {

        allAppList.setVisibility(View.VISIBLE);

        allAppList.removeAllViews();

        PackageManager packageManager = getPackageManager();

        Intent activityIntent = new Intent(Intent.ACTION_MAIN, null);
        activityIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appliste = packageManager.queryIntentActivities(activityIntent, 0);

        String[] erlaubteApps = Config.getInstance().getString();

        int applicationZähler = 0;
        int applicationMenge = appliste.size();
        int validierteAnwendung = 0;

        if (erlaubteApps.length != 0) {


            while (applicationZähler != applicationMenge) {

                LinearLayout newLine = new LinearLayout(getApplicationContext());
                newLine.setOrientation(LinearLayout.HORIZONTAL);
                newLine.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                int position = 0;

                while (position != 3) {
                    try {
                        ResolveInfo info = appliste.get(applicationZähler);

                        int aktuelleAktivierteApp = 0;

                        while (aktuelleAktivierteApp != Config.getInstance().getString().length) {

                            if (erlaubteApps[aktuelleAktivierteApp].toString().contains(info.activityInfo.packageName)) {


                                if (erlaubteApps[aktuelleAktivierteApp].toString().equals(info.activityInfo.packageName)){
                                    validierteAnwendung++;
                                    LinearLayout weightLayout = new LinearLayout(getApplicationContext());
                                    weightLayout.setOrientation(LinearLayout.HORIZONTAL);
                                    weightLayout.setGravity(Gravity.CENTER);
                                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    lp.weight = 1;
                                    weightLayout.setLayoutParams(lp);
                                    final CustomApp customApp = new CustomApp(getApplicationContext());
                                    customApp.setData(info.loadLabel(packageManager).toString(), info.loadIcon(packageManager), info.activityInfo.packageName, info.activityInfo.name);
                                    customApp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            try {
                                                CurrentState.getInstance().setAllowOpening(true);
                                                Intent intent = new Intent();
                                                intent.setComponent(new ComponentName(customApp.getPackageName(), customApp.getClassName()));
                                                startActivity(intent);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    weightLayout.addView(customApp);
                                    newLine.addView(weightLayout);
                                    position++;
                                    if (validierteAnwendung == Config.getInstance().getString().length) {
                                        position = 3;
                                    }
                                }else {

                                    for (ResolveInfo data : appliste){

                                        if (data.activityInfo.name.equals(erlaubteApps[aktuelleAktivierteApp].toString())){
                                            validierteAnwendung++;
                                            LinearLayout weightLayout = new LinearLayout(getApplicationContext());
                                            weightLayout.setOrientation(LinearLayout.HORIZONTAL);
                                            weightLayout.setGravity(Gravity.CENTER);
                                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                                            lp.weight = 1;
                                            weightLayout.setLayoutParams(lp);
                                            final CustomApp customApp = new CustomApp(getApplicationContext());
                                            customApp.setData(data.loadLabel(packageManager).toString(), data.loadIcon(packageManager), info.activityInfo.packageName, erlaubteApps[aktuelleAktivierteApp].toString());
                                            customApp.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    try {
                                                        CurrentState.getInstance().setAllowOpening(true);
                                                        Intent intent = new Intent();
                                                        intent.setComponent(new ComponentName(customApp.getPackageName(), customApp.getClassName()));
                                                        startActivity(intent);
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            });
                                            weightLayout.addView(customApp);
                                            newLine.addView(weightLayout);
                                            position++;
                                            if (validierteAnwendung == Config.getInstance().getString().length) {
                                                position = 3;
                                            }
                                        }

                                    }



                                }




                            }
                            aktuelleAktivierteApp++;
                        }
                        applicationZähler++;

                    } catch (Exception e) {
                        //Toaster.getInstance().showToast(getApplicationContext(),applicationZähler + "");
                        position = 3;
                        e.printStackTrace();
                    }

                }
                allAppList.addView(newLine);

                if (validierteAnwendung == Config.getInstance().getString().length) {

                    if (Config.getInstance().isEnterpriseBrowser()) {

                        CustomApp enterpriseBrowser = new CustomApp(getApplicationContext());
                        Drawable web = getResources().getDrawable(R.drawable.enterprisebrowser);
                        enterpriseBrowser.setData("Enterprise Browser", web, "", "");

                        enterpriseBrowser.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent showEnterpriseBrowser = new Intent(MainActivity.this, BrowserActivity.class);
                                try {
                                    CurrentState.getInstance().setAllowOpening(true);
                                    startActivity(showEnterpriseBrowser);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                        allAppList.addView(enterpriseBrowser);

                    }

                    break;
                }
            }

        } else {
            if (Config.getInstance().isEnterpriseBrowser()) {

                CustomApp enterpriseBrowser = new CustomApp(getApplicationContext());
                Drawable web = getResources().getDrawable(R.drawable.enterprisebrowser);
                enterpriseBrowser.setData("Enterprise Browser", web, "", "");

                enterpriseBrowser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent showEnterpriseBrowser = new Intent(MainActivity.this, BrowserActivity.class);
                        try {
                            CurrentState.getInstance().setAllowOpening(true);
                            startActivity(showEnterpriseBrowser);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

                allAppList.addView(enterpriseBrowser);

            }
        }


        versionTv.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();

        hideAll();

        if (CurrentState.getInstance().getLicenceAktive()) {
            if (SettingsParser.readConfig(getApplicationContext())) {
                versionTv.setVisibility(View.GONE);
            }

            if (CurrentState.getInstance().getAdminMode()) {
                restartBtn.setVisibility(View.VISIBLE);
                infoBtn.setVisibility(View.VISIBLE);
                packageBtn.setVisibility(View.VISIBLE);
                exitAdminBtn.setImageResource(R.drawable.adminoff);
            } else {
                infoBtn.setVisibility(View.GONE);
                restartBtn.setVisibility(View.GONE);
                packageBtn.setVisibility(View.GONE);
                exitAdminBtn.setImageResource(R.drawable.adminon);
                CurrentState.getInstance().setAllowOpening(false);
            }

            new LoadApplications().execute(MainActivity.this);
            allAppList.setVisibility(View.VISIBLE);

        } else {
            new LoadApplications().execute(MainActivity.this);
            allAppList.setVisibility(View.VISIBLE);
            packageBtn.setVisibility(View.VISIBLE);
        }


    }

    private void hideAll() {

        allAppList.setVisibility(View.GONE);
    }

    private void onClicks() {

        exitAdminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (CurrentState.getInstance().getAdminMode() && CurrentState.getInstance().getConfigAvailable()) {
                    CurrentState.getInstance().setAdminMode(false);
                    CurrentState.getInstance().setShowPackages(false);
                    CurrentState.getInstance().setAllowOpening(false);
                    exitAdminBtn.setImageResource(R.drawable.adminon);
                    restartBtn.setVisibility(View.GONE);
                    infoBtn.setVisibility(View.INVISIBLE);
                    packageBtn.setVisibility(View.INVISIBLE);


                    new LoadApplications().execute(MainActivity.this);

                } else if (CurrentState.getInstance().getConfigAvailable()) {


                    PasswordDialog passwordDialog = new PasswordDialog(MainActivity.this);
                    passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    passwordDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (CurrentState.getInstance().getAdminMode()) {


                                packageBtn.setVisibility(View.VISIBLE);
                                exitAdminBtn.setImageResource(R.drawable.adminoff);


                                restartBtn.setVisibility(View.VISIBLE);
                                infoBtn.setVisibility(View.VISIBLE);
                                allAppList.removeAllViews();

                                final TextView textView = new TextView(getApplicationContext());
                                textView.setText("Installierte Apps werden geladen...");

                                allAppList.addView(textView);

                                new LoadApplications().execute(MainActivity.this);

                            }
                        }
                    });
                    passwordDialog.show();

                }
            }
        });

        restartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = getBaseContext().getPackageManager().
                        getLaunchIntentForPackage(getBaseContext().getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();

            }
        });

        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent showInfo = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(showInfo);

            }
        });

        packageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (CurrentState.getInstance().getShowPackages()) {

                    CurrentState.getInstance().setShowPackages(false);
                    allAppList.removeAllViews();

                    final TextView textView = new TextView(getApplicationContext());
                    textView.setText("Installierte Apps werden geladen...");

                    allAppList.addView(textView);
                    new LoadApplications().execute(MainActivity.this);
                } else {
                    CurrentState.getInstance().setShowPackages(true);
                    allAppList.removeAllViews();

                    final TextView textView = new TextView(getApplicationContext());
                    textView.setText("Installierte Apps werden geladen...");

                    allAppList.addView(textView);
                    new LoadApplications().execute(MainActivity.this);
                }

            }
        });

    }

    private void findViews() {

        allAppList = (LinearLayout) findViewById(R.id.appList);
        adminViewLl = (LinearLayout) findViewById(R.id.adminViewLl);
        infoBtn = (ImageButton) findViewById(R.id.infoBtn);
        exitAdminBtn = (ImageButton) findViewById(R.id.exitBtn);
        packageBtn = (ImageButton) findViewById(R.id.packageBtn);
        versionTv = (TextView) findViewById(R.id.versionTv);
        restartBtn = (ImageButton) findViewById(R.id.restartBtn);
    }

    @Override
    public void onBackPressed() {
        return;
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (!CurrentState.getInstance().getAdminMode()) {

            if (!CurrentState.getInstance().getAllowOpening()) {

                ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);

                activityManager.moveTaskToFront(getTaskId(), 0);

                PasswordDialog passwordDialog = new PasswordDialog(this);
                passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                passwordDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (CurrentState.getInstance().getAdminMode()) {
                            adminViewLl.setVisibility(View.VISIBLE);
                            new LoadApplications().execute(MainActivity.this);

                        }
                    }
                });
                passwordDialog.show();
            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == 25) {
            return false;
        }

        return true;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        currentFocus = hasFocus;

        if (!hasFocus) {

            if (!CurrentState.getInstance().getAdminMode()) {
                collapseNow();
            }
        }
    }

    private void collapseNow() {

        if (collapseNotificationHandler == null) {
            collapseNotificationHandler = new Handler();
        }

        if (!currentFocus && !isPaused) {

            collapseNotificationHandler.postDelayed(new Runnable() {

                @Override
                public void run() {

                    Object statusBarService = getSystemService("statusbar");
                    Class<?> statusBarManager = null;

                    try {
                        statusBarManager = Class.forName("android.app.StatusBarManager");
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    Method collapseStatusBar = null;

                    try {

                        if (Build.VERSION.SDK_INT > 16) {
                            collapseStatusBar = statusBarManager.getMethod("collapsePanels");
                        } else {
                            collapseStatusBar = statusBarManager.getMethod("collapse");
                        }
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }

                    collapseStatusBar.setAccessible(true);

                    try {
                        collapseStatusBar.invoke(statusBarService);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    if (!currentFocus && !isPaused) {
                        collapseNotificationHandler.postDelayed(this, 100L);
                    }

                }
            }, 300L);
        }

    }

    private class LoadApplications extends AsyncTask<Context, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(final Context... contexts) {

            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (CurrentState.getInstance().getShowPackages()) {
                            showPackages();
                        } else {
                            if (CurrentState.getInstance().getAdminMode()) {
                                loadAllApps();
                            } else {
                                loadEnabledApps();
                            }
                        }


                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


        }
    }

    private void showPackages() {

        allAppList.setVisibility(View.VISIBLE);

        allAppList.removeAllViews();

        PackageManager packageManager = getPackageManager();

        Intent activityIntent = new Intent(Intent.ACTION_MAIN, null);
        activityIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> appliste = packageManager.queryIntentActivities(activityIntent, 0);

        int applicationZähler = 0;
        int applicationMenge = appliste.size();

        while (applicationZähler != applicationMenge) {

            LinearLayout newLine = new LinearLayout(getApplicationContext());
            newLine.setOrientation(LinearLayout.HORIZONTAL);
            newLine.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            int position = 0;
            try {

                ResolveInfo info = appliste.get(applicationZähler);

                LinearLayout weightLayout = new LinearLayout(getApplicationContext());
                weightLayout.setOrientation(LinearLayout.HORIZONTAL);
                weightLayout.setGravity(Gravity.CENTER);

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.weight = 1;
                weightLayout.setLayoutParams(lp);

                int count = 0;

                for (ResolveInfo data : appliste){

                    if (data.activityInfo.packageName.equals(info.activityInfo.packageName)){
                        count++;
                    }

                }

                final CustomAppList customApp = new CustomAppList(getApplicationContext());

                if (count > 1){
                    customApp.setData(info.loadLabel(packageManager).toString(), info.loadIcon(packageManager), info.activityInfo.name, info.activityInfo.name);
                }else {
                    customApp.setData(info.loadLabel(packageManager).toString(), info.loadIcon(packageManager), info.activityInfo.name, info.activityInfo.packageName);
                }

                weightLayout.addView(customApp);

                newLine.addView(weightLayout);
                applicationZähler++;

            } catch (Exception e) {
                e.printStackTrace();
            }

            allAppList.addView(newLine);
        }


    }


}
