package dev.jhsoftware.com.pike.Settings;

/**
 * Created by herold on 12.03.2018.
 */

public class Config {
    private static final Config ourInstance = new Config();

    private String Passwort;
    private String URL;
    private boolean EnterpriseBrowser;
    private String[] String;
    private String Version;
    private boolean Refresh;
    private boolean ShowBrowserToolbar = false;
    private boolean EnableBackButton;
    private boolean ShowHomeButton;
    private boolean EnableJavascript;
    private int Zoom;
    private boolean EnableZoom;
    private boolean PinchToZoomButtons;
    private String DisplayMode = "Portrait";



    public static Config getInstance() {
        return ourInstance;
    }

    private Config() {

    }

    public boolean isShowBrowserToolbar() {
        return ShowBrowserToolbar;
    }
    public void setShowBrowserToolbar(boolean showBrowserToolbar) {
        ShowBrowserToolbar = showBrowserToolbar;
    }

    public String getPasswort() {
        return Passwort;
    }
    public void setPasswort(String passwort) {
        Passwort = passwort;
    }

    public String getURL() {
        return URL;
    }
    public void setURL(String URL) {
        this.URL = URL;
    }

    public boolean isEnterpriseBrowser() {
        return EnterpriseBrowser;
    }
    public void setEnterpriseBrowser(boolean enterpriseBrowser) {
        EnterpriseBrowser = enterpriseBrowser;
    }

    public java.lang.String[] getString() {
        return String;
    }
    public void setString(java.lang.String[] string) {
        String = string;
    }

    public String getVersion() {
        return Version;
    }
    public void setVersion(java.lang.String version) {
        Version = version;
    }

    public boolean isRefresh() {
        return Refresh;
    }
    public void setRefresh(boolean refresh) {
        Refresh = refresh;
    }

    public boolean isEnableBackButton() {
        return EnableBackButton;
    }
    public void setEnableBackButton(boolean enableBackButton) {
        EnableBackButton = enableBackButton;
    }

    public boolean isShowHomeButton() {
        return ShowHomeButton;
    }
    public void setShowHomeButton(boolean showHomeButton) {
        ShowHomeButton = showHomeButton;
    }

    public boolean isEnableJavascript() {
        return EnableJavascript;
    }
    public void setEnableJavascript(boolean enableJavascript) {
        EnableJavascript = enableJavascript;
    }

    public int getZoom() {
        return Zoom;
    }
    public void setZoom(int zoom) {
        Zoom = zoom;
    }

    public boolean isEnableZoom() {
        return EnableZoom;
    }
    public void setEnableZoom(boolean enableZoom) {
        EnableZoom = enableZoom;
    }

    public boolean isPinchToZoomButtons() {
        return PinchToZoomButtons;
    }
    public void setPinchToZoomButtons(boolean pinchToZoomButtons) {
        PinchToZoomButtons = pinchToZoomButtons;
    }

    public java.lang.String getDisplayMode() {
        return DisplayMode;
    }
    public void setDisplayMode(java.lang.String displayMode) {
        DisplayMode = displayMode;
    }
}
