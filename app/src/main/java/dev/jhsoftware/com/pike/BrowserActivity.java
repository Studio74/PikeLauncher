package dev.jhsoftware.com.pike;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.Timer;

import dev.jhsoftware.com.pike.Dialogs.PasswordDialog;
import dev.jhsoftware.com.pike.Settings.Config;
import dev.jhsoftware.com.pike.Settings.CurrentState;

public class BrowserActivity extends Activity {

    WebView enterpriseWv;
    ProgressBar webPb;
    ImageButton gotoBtn, homeBtn, refreshBtn, exitBtn;
    EditText urlEt;
    LinearLayout adminView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);


        if (Config.getInstance().getDisplayMode().equals("Portrait")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (Config.getInstance().getDisplayMode().equals("Landscape")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else if (Config.getInstance().getDisplayMode().equals("Auto")){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }




        findViews();
        onClicks();
        setUp();

        Timer timer = new Timer();
        timer.schedule(new TimerTask(), 0, 1000);
    }


    private void setUp() {

        hideBottom();


        if (!Config.getInstance().isShowBrowserToolbar()) {
            adminView.setVisibility(View.GONE);
        }

        enterpriseWv.getSettings().setJavaScriptEnabled(Config.getInstance().isEnableJavascript());
        enterpriseWv.loadUrl(Config.getInstance().getURL());
        enterpriseWv.getSettings().setDisplayZoomControls(Config.getInstance().isPinchToZoomButtons());
        enterpriseWv.getSettings().setBuiltInZoomControls(Config.getInstance().isEnableZoom());
        urlEt.setText(Config.getInstance().getURL());

        if (Config.getInstance().isRefresh()) {
            refreshBtn.setVisibility(View.VISIBLE);
        } else {
            refreshBtn.setVisibility(View.GONE);
        }

        if (Config.getInstance().isShowHomeButton()) {
            homeBtn.setVisibility(View.VISIBLE);
        } else {
            homeBtn.setVisibility(View.GONE);
        }

        enterpriseWv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                hideBottom();
                enterpriseWv.loadUrl(request.getUrl().toString());
                urlEt.setText(request.getUrl().toString());

                return false;
            }


        });

        enterpriseWv.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                webPb.setProgress(newProgress);

                if (newProgress == 100) {
                    webPb.setProgress(0);
                }
            }
        });

        enterpriseWv.setInitialScale(Config.getInstance().getZoom());


        if (CurrentState.getInstance().getAdminMode()) {
            urlEt.setVisibility(View.VISIBLE);
            gotoBtn.setVisibility(View.VISIBLE);
        } else {
            urlEt.setVisibility(View.GONE);
            gotoBtn.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        hideBottom();
        if (enterpriseWv.canGoBack() && Config.getInstance().isEnableBackButton()) {
            enterpriseWv.goBack();
        } else {
            return;
        }
        return;
    }

    private void hideBottom() {

        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
    }

    private void onClicks() {

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterpriseWv.loadUrl(Config.getInstance().getURL());
            }
        });

        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {



                    final Intent homeScreen = new Intent(BrowserActivity.this, MainActivity.class);

                    if (Config.getInstance().getString().length == 0) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                PasswordDialog passwordDialog = new PasswordDialog(BrowserActivity.this);
                                passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                passwordDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        if (CurrentState.getInstance().getAdminMode()) {
                                            CurrentState.getInstance().setAdminMode(true);
                                            finish();
                                        }
                                    }
                                });
                                passwordDialog.show();
                            }
                        });

                    } else {
                        finish();
                    }
                }catch (Exception e){
                    Toaster.getInstance().showToast(getApplicationContext(), e.toString());
                }


            }
        });

        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterpriseWv.reload();
            }
        });

        if (!Config.getInstance().isShowBrowserToolbar()) {

            webPb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adminView.setVisibility(View.VISIBLE);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            adminView.setVisibility(View.GONE);
                        }
                    }, 5000);
                }
            });
        }
        gotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!CurrentState.getInstance().getAdminMode()) {
                    if (!urlEt.getText().toString().isEmpty()) {
                        try {
                            enterpriseWv.loadUrl(urlEt.getText().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toaster.getInstance().showToast(getApplicationContext(), e.toString());
                        }
                    }
                } else {
                    Toaster.getInstance().showToast(getApplicationContext(), "Du musst Admin sein um eine Seite zu öffnen");
                }

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();


        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);

    }

    private void findViews() {
        enterpriseWv = (WebView) findViewById(R.id.enterpriseWv);
        webPb = (ProgressBar) findViewById(R.id.webPb);
        urlEt = (EditText) findViewById(R.id.urlEt);
        refreshBtn = (ImageButton) findViewById(R.id.refreshBtn);
        exitBtn = (ImageButton) findViewById(R.id.exitBtn);
        gotoBtn = (ImageButton) findViewById(R.id.gotoBtn);
        homeBtn = (ImageButton) findViewById(R.id.homeBtn);
        adminView = (LinearLayout) findViewById(R.id.adminViewLl);

    }


    class TimerTask extends java.util.TimerTask {

        @Override
        public void run() {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideBottom();
                }
            });


        }
    }

}
