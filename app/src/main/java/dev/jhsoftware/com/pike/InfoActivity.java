package dev.jhsoftware.com.pike;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import dev.jhsoftware.com.pike.Logic.LizenzLogic;
import dev.jhsoftware.com.pike.Settings.CurrentState;

public class InfoActivity extends Activity {

    TextView freischaltcodeTv, expireTv, seriennummerTv;
    LinearLayout freischaltcodeLl, lizenzAktiviertLl;
    EditText freischaltcodeEt;
    Button freischaltcodeBtn;
    ImageButton backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);




        if (Build.VERSION.SDK_INT >= 23) {
            int readCheck = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            if (readCheck != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }

            int telefonCheck = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            if (telefonCheck != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
            }
        }


        findViews();
        onClicks();



        seriennummerTv.setText("Seriennummer: " + Build.SERIAL);
    }

    private void onClicks() {


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    Intent showHomeScreen = new Intent(InfoActivity.this, MainActivity.class);
                    startActivity(showHomeScreen);


            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void findViews() {
        freischaltcodeBtn = (Button) findViewById(R.id.freischaltcodeBtn);
        seriennummerTv = (TextView) findViewById(R.id.seriennummerTv);
        freischaltcodeEt = (EditText) findViewById(R.id.freischaltcodeEt);
        freischaltcodeLl = (LinearLayout) findViewById(R.id.freischaltcodeLl);
        freischaltcodeTv = (TextView) findViewById(R.id.freischaltcodeTv);
        backBtn = (ImageButton) findViewById(R.id.backBtn);
        expireTv = (TextView) findViewById(R.id.expireTv);
        lizenzAktiviertLl = (LinearLayout) findViewById(R.id.lizenzAktiviertLl);
    }

}
