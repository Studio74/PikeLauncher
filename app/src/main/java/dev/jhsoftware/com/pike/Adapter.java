package dev.jhsoftware.com.pike;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by herold on 14.03.2018.
 */

public class Adapter extends ArrayAdapter<ApplicationInfo> {

    private List<ApplicationInfo> appsList = null;
    private Context context;
    private PackageManager packageManager;

    public Adapter(Context context, int resource, List<ApplicationInfo> appsList) {
        super(context, resource);

        this.context = context;
        this.appsList = appsList;
        this.packageManager = context.getPackageManager();
    }

    @Override
    public int getCount() {
        return ((null != appsList) ? appsList.size() : 0);
    }

    @Override
    public ApplicationInfo getItem(int position) {
        return ((null != appsList) ? appsList.get(position) : null);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (null == view){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.custom_app, null);
        }

        ApplicationInfo applicationInfo = appsList.get(position);
        if (null != applicationInfo){
            TextView appName = (TextView) view.findViewById(R.id.name);
            ImageView appIcon = (ImageView) view.findViewById(R.id.icon);

            appName.setText(applicationInfo.loadLabel(packageManager));
            appIcon.setImageDrawable(applicationInfo.loadIcon(packageManager));
        }

        return view;
    }
}
